#version 410 core

uniform float window_height;
uniform float window_width;

uniform sampler2D color;
uniform sampler2D normal;
uniform mat4 inv_projection_matrix;

#define NR_LIGHTS 1
uniform vec4 light_pos[NR_LIGHTS];
uniform vec3 light_col[NR_LIGHTS];

vec3 VSPositionFromDepth(vec2 texcoord, float z)
{
    // Get x/w and y/w from the viewport position
    vec4 positionNDC = vec4(2 * vec3(texcoord, z) - 1, 1.f);
    // Transform by the inverse projection matrix
    vec4 positionVS = inv_projection_matrix * positionNDC;
    // Divide by w to get the view-space position
    return positionVS.xyz / positionVS.w;  
}

out vec4 out_color;

vec3 phong(vec3 n, vec3 l, vec3 v, vec3 diffuse_color, vec3 specular_color,
    float specular_coef, float exponent, vec3 light_color) {
    float dotProd = max(0, dot(n, l));
    vec3 diffuse = diffuse_color * dotProd;
    vec3 specular = vec3(0.);
    if (dotProd > 0) {
        // Blinn-Phong
        vec3 h = normalize(l + v);
        specular = specular_color * pow(max(0, dot(h, n)), exponent * 4);
    }
    return 0.1 * diffuse_color + // ambiant color
    0.9 * (diffuse + specular_coef * specular) * light_color;
}

void main()
{
    vec2 window_size = vec2(window_width,window_height);
    vec2 texcoord = vec2(gl_FragCoord) / window_size ;

    vec4 texnormal = texture(normal,texcoord);
    vec4 texcolor = texture(color,texcoord);
    float depht = texnormal.w;
    vec3 PositionFromDepth = VSPositionFromDepth(texcoord,depht );
    
    for(int i = 0; i < NR_LIGHTS; i++) {
        vec3 l = light_pos[i].xyz - PositionFromDepth;
        out_color += vec4(phong(
                        normalize(texnormal.xyz)
                        , normalize(l)
                        , -normalize(PositionFromDepth)
                        , texcolor.rgb
                        , vec3(1.0)
                        , texcolor.z
                        , 5
                        ,light_col[i] / max(1, dot(l, l))
                        )
                    ,1);
    }
        
}