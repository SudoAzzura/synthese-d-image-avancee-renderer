#version 410 core

in vec3 normalV;
in vec3 colorV;

uniform mat4 view_matrix;
uniform mat3 normal_matrix;
uniform float specular_coef;

layout (location = 0)out vec4 out_color_s;
layout (location = 1)out vec4 out_normal_z;

void main()
{
    out_color_s.rgb = colorV;
    out_color_s.a = specular_coef;
    out_normal_z.xyz = normalize(normalV);
    out_normal_z.w = gl_FragCoord.z;
}