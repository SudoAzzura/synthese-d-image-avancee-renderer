#version 410 core

in vec4 vtx_position;

void main() {
    gl_Position = vtx_position * 2 - 1 ;

}