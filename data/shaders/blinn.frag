#version 410 core
#define NR_LIGHTS 1

uniform vec4 light_pos[NR_LIGHTS];
uniform vec3 light_col[NR_LIGHTS];


uniform mat4 view_matrix;
uniform mat3 normal_matrix;

uniform float specular_coef;

in vec3 vertexV;
in vec3 normalV;
in vec3 colorV;

out vec4 out_color;

vec3 phong(vec3 n, vec3 l, vec3 v, vec3 diffuse_color, vec3 specular_color,
           float specular_coef, float exponent, vec3 light_color) {
  float dotProd = max(0, dot(n, l));
  vec3 diffuse = diffuse_color * dotProd;
  vec3 specular = vec3(0.);
  if (dotProd > 0) {
    // Blinn-Phong
    vec3 h = normalize(l + v);
    specular = specular_color * pow(max(0, dot(h, n)), exponent * 4);
  }
  return 0.1 * diffuse_color + // ambiant color
         0.9 * (diffuse + specular_coef * specular) * light_color;
}

void main() {
  for(int i = 0; i < NR_LIGHTS; i++) {
    out_color += vec4(phong(normalize(normalV), normalize(vec3(light_pos[i])- vertexV), -normalize(vertexV),
                         colorV.rgb, vec3(1.0), specular_coef, 5,
                         light_col[i] / max(1, dot(vec3(light_pos[i])- vertexV, vec3(light_pos[i])- vertexV))),
                   1);

  }

  
}
