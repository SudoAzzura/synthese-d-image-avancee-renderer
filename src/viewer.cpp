#include "viewer.h"
#include "camera.h"
#include "fbo.h"
#include "mesh.h"

#include <SOIL2.h>
#include <imgui.h>

using namespace Eigen;

Viewer::Viewer() {}

Viewer::~Viewer() {
  for (Mesh *s : _shapes)
    delete s;
  for (Mesh *s : _pointLights)
    delete s;
}

// initialize OpenGL context just for shadow: 1 sphere and 1 light

void Viewer::init(int w, int h) {
  _winWidth = w;
  _winHeight = h;

  _fbo.init(_winWidth,_winHeight);

  //Partie 1 2.Calcul éclairage 1.
  _quad = new Mesh();
  _quad->createGrid(2, 2);
  _quad->init();

  // set the background color, i.e., the color used
  // to fill the screen when calling glClear(GL_COLOR_BUFFER_BIT)
  glClearColor(0.f, 0.f, 0.f, 1.f);

  glEnable(GL_DEPTH_TEST);

  loadProgram();

  Mesh *sphere = new Mesh();
  sphere->load(DATA_DIR "/models/sphere.off");
  sphere->init();
  sphere->transformationMatrix() = Translation3f(0.f, 0.f, 1.f) * Scaling(0.5f);
  _shapes.push_back(sphere);
  _specularCoef.push_back(0.3f);

  _lightColors.push_back(Vector3f::Constant(0.8f));
  Mesh *light = new Mesh();
  light->createSphere(0.025f);
  light->init();
  light->transformationMatrix() =  Translation3f(1.f, 1.f, 1.f) ;

      
  _pointLights.push_back(light);
  //Shadow
  //pour une seule lumière c'est plus simple
  Vector3f lightPos0 = _pointLights[0]->transformationMatrix().translation();
  //on test qu'une sphere et une lumière
  //for (size_t i = 1; i < _shapes.size(); ++i){
    //std::cout << "shape:" << _shapes[0] << std::endl;
    //juste pour la sphere
    // PB : ne pas oublier de prendre en compte la matrice de transformation de l'objet
    Mesh *m = sphere->computeShadowVolume(lightPos0);
    m->init();
    m->transformationMatrix() = sphere->transformationMatrix()* light->transformationMatrix();

    _shadowColors.push_back(Vector3f(0, 1, 0));
    _shadow_shapes.push_back(m);
  //}
  _lastTime = glfwGetTime();

  AlignedBox3f aabb;
  
  for (size_t i = 0; i < _shapes.size(); ++i)
    aabb.extend(_shapes[i]->boundingBox());
    
  _cam.setSceneCenter(aabb.center());
  _cam.setSceneRadius(aabb.sizes().maxCoeff());
  _cam.setSceneDistance(_cam.sceneRadius() * 3.f);
  _cam.setMinNear(0.1f);
  _cam.setNearFarOffsets(-_cam.sceneRadius() * 100.f,
                         _cam.sceneRadius() * 100.f);
  _cam.setScreenViewport(AlignedBox2f(Vector2f(0.0, 0.0), Vector2f(w, h)));
}

/*
////////////////////////////////////////////////////////////////////////////////
// GL stuff
// char * name (int i, char[8] mot,int dec) {
//   const char n [10] = mot + (char)i + ']';
//   return n;
// }
// initialize OpenGL context
void Viewer::init(int w, int h) {
  _winWidth = w;
  _winHeight = h;

  _fbo.init(_winWidth,_winHeight);

  //Partie 1 2.Calcul éclairage 1.
  _quad = new Mesh();
  _quad->createGrid(2, 2);
  _quad->init();

  // set the background color, i.e., the color used
  // to fill the screen when calling glClear(GL_COLOR_BUFFER_BIT)
  glClearColor(0.f, 0.f, 0.f, 1.f);

  glEnable(GL_DEPTH_TEST);

  loadProgram();

  Mesh *quad = new Mesh();
  quad->createGrid(2, 2);
  quad->init();
  quad->transformationMatrix() = AngleAxisf(M_PI / 2.f, Vector3f(-1, 0, 0)) *
                                 Scaling(20.f, 20.f, 1.f) *
                                 Translation3f(-0.5, -0.5, -0.5);
  _shapes.push_back(quad);
  _specularCoef.push_back(0.f);

  Mesh *tw = new Mesh();
  tw->load(DATA_DIR "/models/tw.off");
  tw->init();
  _shapes.push_back(tw);
  _specularCoef.push_back(0.75);

  Mesh *tw1 = new Mesh();
  tw1->load(DATA_DIR "/models/tw.off");
  tw1->init();
  tw1->transformationMatrix() = Translation3f(0, 0, 1.f);
  _shapes.push_back(tw1);
  _specularCoef.push_back(0.75);

  Mesh *tw2 = new Mesh();
  tw2->load(DATA_DIR "/models/tw.off");
  tw2->init();
  tw2->transformationMatrix() = Translation3f(0, 0, 3.f);
  _shapes.push_back(tw2);
  _specularCoef.push_back(0.75);


  Mesh *tw5 = new Mesh();
  tw5->load(DATA_DIR "/models/tw.off");
  tw5->init();
  tw5->transformationMatrix() = Translation3f(1.f, 1.f, 0);
  _shapes.push_back(tw5);
  _specularCoef.push_back(0.75);

  Mesh *tw6 = new Mesh();
  tw6->load(DATA_DIR "/models/tw.off");
  tw6->init();
  tw6->transformationMatrix() = Translation3f(2.f, 1.f, 0);
  _shapes.push_back(tw6);
  _specularCoef.push_back(0.75);

  Mesh *tw7 = new Mesh();
  tw7->load(DATA_DIR "/models/tw.off");
  tw7->init();
  tw7->transformationMatrix() = Translation3f(0, 1.f, 0);
  _shapes.push_back(tw7);
  _specularCoef.push_back(0.75);

//THE ARMY
  // for (int i = 0; i<15;i++) {
  //   for (int j = 0; j<15;j++) {
  //     Mesh *tw13 = new Mesh();
  //     tw13->load(DATA_DIR "/models/tw.off");
  //     tw13->init();
  //     tw13->transformationMatrix() = Translation3f(i, 0, j);
  //     _shapes.push_back(tw13);
  //     _specularCoef.push_back(0.75);
  //   }
  // }

  Mesh *sphere = new Mesh();
  sphere->load(DATA_DIR "/models/sphere.off");
  sphere->init();
  sphere->transformationMatrix() = Translation3f(0, 0, 2.f) * Scaling(0.5f);
  _shapes.push_back(sphere);
  _specularCoef.push_back(0.3f);




  _lightColors.push_back(Vector3f::Constant(0.8f));
  Mesh *light = new Mesh();
  light->createSphere(0.025f);
  light->init();
  light->transformationMatrix() =
      Translation3f(_cam.sceneCenter() +
                    _cam.sceneRadius() *
                        Vector3f(Eigen::internal::random<float>(),
                                  Eigen::internal::random<float>(0.1f, 0.5f),
                                  Eigen::internal::random<float>()));
  _pointLights.push_back(light);

  // nouvelle lumière
  

  _lightColors.push_back(Vector3f(0, 1, 1));
  Mesh *light2 = new Mesh();
  light2->createSphere(0.025f);
  light2->init();
  light2->transformationMatrix() =
      Translation3f(_cam.sceneCenter() +
                    _cam.sceneRadius() *
                        Vector3f(Eigen::internal::random<float>(),
                                  Eigen::internal::random<float>(0.1f, 0.4f),
                                  Eigen::internal::random<float>()));
  _pointLights.push_back(light2);

//3eme lumière
  _lightColors.push_back(Vector3f(1, 0.5, 0.1));
  Mesh *light3 = new Mesh();
  light3->createSphere(0.025f);
  light3->init();
  light3->transformationMatrix() =
      Translation3f(_cam.sceneCenter() +
                    _cam.sceneRadius() *
                        Vector3f(Eigen::internal::random<float>(),
                                  Eigen::internal::random<float>(0.1f, 0.2f),
                                  Eigen::internal::random<float>()));
  _pointLights.push_back(light3);

  // for (int i = 0; i<14;i++) {
  //   _lightColors.push_back(Vector3f(0.5, 0, 0.5));
  //   Mesh *lightmult = new Mesh();
  //   lightmult->createSphere(0.025f);
  //   lightmult->init();
  //   lightmult->transformationMatrix() =
  //       Translation3f(Vector3f(Eigen::internal::random<float>(),
  //                                   Eigen::internal::random<float>(1.f, 0.2f),
  //                                   Eigen::internal::random<float>()));
  //   _pointLights.push_back(lightmult);
  // }

  //4eme lumière
  _lightColors.push_back(Vector3f(1, 0, 0.5));
  Mesh *light4 = new Mesh();
  light4->createSphere(0.125f);
  light4->init();
  light4->transformationMatrix() =
      Translation3f(_cam.sceneCenter() +
                    _cam.sceneRadius() *
                        Vector3f(Eigen::internal::random<float>(0.2f,0.4f),
                                  Eigen::internal::random<float>(0.1f, 0.2f),
                                  Eigen::internal::random<float>()));
  light4->transformationMatrix() = Translation3f(0, 0, 10.f);
  _pointLights.push_back(light4);

  _lightColors.push_back(Vector3f(0.1, 0.23, 0.7));
  Mesh *light5 = new Mesh();
  light5->createSphere(0.125f);
  light5->init();
  light5->transformationMatrix() = Translation3f(5, 0, 8.f);
  light5->transformationMatrix() =
      Translation3f(_cam.sceneCenter() +
                    _cam.sceneRadius() *
                        Vector3f(Eigen::internal::random<float>(0.2f,0.1f),
                                  Eigen::internal::random<float>(0.1f, 0.2f),
                                  Eigen::internal::random<float>()));
  
  _pointLights.push_back(light5);


  _lastTime = glfwGetTime();

  AlignedBox3f aabb;
  for (size_t i = 0; i < _shapes.size(); ++i)
    aabb.extend(_shapes[i]->boundingBox());

  _cam.setSceneCenter(aabb.center());
  _cam.setSceneRadius(aabb.sizes().maxCoeff());
  _cam.setSceneDistance(_cam.sceneRadius() * 3.f);
  _cam.setMinNear(0.1f);
  _cam.setNearFarOffsets(-_cam.sceneRadius() * 100.f,
                         _cam.sceneRadius() * 100.f);
  _cam.setScreenViewport(AlignedBox2f(Vector2f(0.0, 0.0), Vector2f(w, h)));
}
*/
void Viewer::reshape(int w, int h) {
  _winWidth = w;
  _winHeight = h;
  _cam.setScreenViewport(AlignedBox2f(Vector2f(0.0, 0.0), Vector2f(w, h)));
  glViewport(0, 0, w, h);
}

void Viewer::drawForward() {
  glDepthMask(GL_TRUE);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
  drawShadow();
  _blinnPrg.activate();
  glUniformMatrix4fv(_blinnPrg.getUniformLocation("projection_matrix"), 1,
                      GL_FALSE, _cam.computeProjectionMatrix().data());
  glUniformMatrix4fv(_blinnPrg.getUniformLocation("view_matrix"), 1, GL_FALSE,
                      _cam.computeViewMatrix().data());

  //plusieurs lumières possibles stockées           
  for(int i = 0; i< _pointLights.size(); i++) {
    Vector4f lightPos;
    char c[14];
    sprintf(c,"light_pos[%d]",i);
    char c2[14];
    sprintf(c2,"light_col[%d]",i);
    lightPos << _pointLights[i]->transformationMatrix().translation(), 1.f;
    glUniform4fv(_blinnPrg.getUniformLocation((const char * )c), 1,
                (_cam.computeViewMatrix() * lightPos).eval().data());
    glUniform3fv(_blinnPrg.getUniformLocation((const char * )c2), 1,
                _lightColors[i].data());
  }

  for (size_t i = 0; i < _shapes.size(); ++i) {
    glUniformMatrix4fv(_blinnPrg.getUniformLocation("model_matrix"), 1,
                        GL_FALSE, _shapes[i]->transformationMatrix().data());
    Matrix3f normal_matrix =
        (_cam.computeViewMatrix() * _shapes[i]->transformationMatrix())
            .linear()
            .inverse()
            .transpose();
    glUniformMatrix3fv(_blinnPrg.getUniformLocation("normal_matrix"), 1,
                        GL_FALSE, normal_matrix.data());
    glUniform1f(_blinnPrg.getUniformLocation("specular_coef"),
                _specularCoef[i]);

    _shapes[i]->draw(_blinnPrg);
  }

  _blinnPrg.deactivate();
    
  drawLights();
  
}

void Viewer::drawDeferred() {
  //activation du gbuffer
  _gbufferPrg.activate();

  //activation du fbo
  _fbo.bind();

  // mise à 0 des buffers
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);  
  
  // stockage de la matrice de projection
  glUniformMatrix4fv(_gbufferPrg.getUniformLocation("projection_matrix"), 1,
                      GL_FALSE, _cam.computeProjectionMatrix().data());
  //stockage de la matrix de view
  glUniformMatrix4fv(_gbufferPrg.getUniformLocation("view_matrix"), 1, GL_FALSE,
                     _cam.computeViewMatrix().data());

  for (size_t i = 0; i < _shapes.size(); ++i) {
    glUniformMatrix4fv(_gbufferPrg.getUniformLocation("model_matrix"), 1,
                       GL_FALSE, _shapes[i]->transformationMatrix().data());
    // remplissage de la matrice des normales
    Matrix3f normal_matrix =
        (_cam.computeViewMatrix() * _shapes[i]->transformationMatrix())
            .linear()
            .inverse()
            .transpose();
    // stockage de la matrice des normales
    glUniformMatrix3fv(_gbufferPrg.getUniformLocation("normal_matrix"), 1,
                       GL_FALSE, normal_matrix.data());
    //stockage du coef spéculair
    glUniform1f(_gbufferPrg.getUniformLocation("specular_coef"),
                _specularCoef[i]);

    _shapes[i]->draw(_gbufferPrg);
  }
  _gbufferPrg.deactivate();
  _fbo.unbind();

  // _fbo.savePNG("color.png", 0);
  
  // _fbo.savePNG("normal.png", 1);
  
  glViewport(0,0,_winWidth,_winHeight);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   //activation du deferred buff
  _deferredPrg.activate();
  

  // glEnable(GL_BLEND);
  // glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  //glDepthFunc(GL_LESS);
  // glDisable(GL_DEPTH_TEST);

  //remplir les lumières

  for(int i = 0; i< _pointLights.size(); i++) {
    Vector4f lightPos;
    char c[14];
    sprintf(c,"light_pos[%d]",i);
    char c2[14];
    sprintf(c2,"light_col[%d]",i);
    lightPos << _pointLights[i]->transformationMatrix().translation(), 1.f;
    glUniform4fv(_deferredPrg.getUniformLocation((const char * )c), 1,
                (_cam.computeViewMatrix() * lightPos).eval().data());
    glUniform3fv(_deferredPrg.getUniformLocation((const char * )c2), 1,
                _lightColors[i].data());
  }

  //sampler2D color du frag
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, _fbo.textures[0]);
  glUniform1i(_deferredPrg.getUniformLocation("color"),0);
  //sampler2D normal du frag
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, _fbo.textures[1]);
  glUniform1i(_deferredPrg.getUniformLocation("normal"),1);
  //window_height
  glUniform1f(_deferredPrg.getUniformLocation("window_height"),
                _winHeight);
  //window_width
  glUniform1f(_deferredPrg.getUniformLocation("window_width"),
                _winWidth);
  // mat projection
  Matrix4f proj_mat = _cam.computeProjectionMatrix();
  // inv mat projection
  Matrix4f inv_proj_mat = proj_mat.inverse();
  //stockage mat4 inv projection matrix
  glUniformMatrix4fv(_deferredPrg.getUniformLocation("inv_projection_matrix"), 1,
                     GL_FALSE,inv_proj_mat.data());

  _quad->draw(_deferredPrg);
  _deferredPrg.deactivate();

   // pour que les lampes ponctuelles s'affichent correctement
  // "au dessus" du rendu par deferred shading
  //activation des deux fbo
  glBindFramebuffer(GL_READ_FRAMEBUFFER, _fbo.id());
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
  glBlitFramebuffer(0, 0, _winWidth, _winHeight, 0, 0, _winWidth, _winHeight, GL_DEPTH_BUFFER_BIT, GL_NEAREST);

  
  drawLights();
  
}

void Viewer::drawLights() {
  _simplePrg.activate();
  glUniformMatrix4fv(_simplePrg.getUniformLocation("projection_matrix"), 1,
                     GL_FALSE, _cam.computeProjectionMatrix().data());
  glUniformMatrix4fv(_simplePrg.getUniformLocation("view_matrix"), 1, GL_FALSE,
                     _cam.computeViewMatrix().data());
  for (int i = 0; i < _pointLights.size(); ++i) {
    Affine3f modelMatrix = _pointLights[i]->transformationMatrix();  
    glUniformMatrix4fv(_simplePrg.getUniformLocation("model_matrix"), 1,
                       GL_FALSE, modelMatrix.data());
    glUniform3fv(_simplePrg.getUniformLocation("light_col"), 1,
                 _lightColors[i].data());

    _pointLights[i]->draw(_simplePrg);
  }
  _simplePrg.deactivate();
}

void Viewer::drawShadow() {

  _simplePrg.activate();
  glUniformMatrix4fv(_simplePrg.getUniformLocation("projection_matrix"), 1,
                     GL_FALSE, _cam.computeProjectionMatrix().data());
  glUniformMatrix4fv(_simplePrg.getUniformLocation("view_matrix"), 1, GL_FALSE,
                     _cam.computeViewMatrix().data());
  //glEnable(GL_CULL_FACE);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  for (int i = 0; i < _shadow_shapes.size(); ++i) {
    Affine3f modelMatrix = _shadow_shapes[i]->transformationMatrix();  
    glUniformMatrix4fv(_simplePrg.getUniformLocation("model_matrix"), 1,
                       GL_FALSE, modelMatrix.data());
    glUniform3fv(_simplePrg.getUniformLocation("light_col"), 1,
                 _shadowColors[i].data());
    
    
    _shadow_shapes[i]->draw(_simplePrg);
    
  }
  //glDisable(GL_CULL_FACE);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  _simplePrg.deactivate();
}


void Viewer::updateScene() {
  if (_animate && glfwGetTime() > _lastTime + 1.f / 60.f) {
    for (int i = 0; i < _pointLights.size(); ++i) {
      // update light position
      Vector3f lightPos = _pointLights[i]->transformationMatrix().translation();
      Vector3f lightDir = (lightPos - _cam.sceneCenter()) / _cam.sceneRadius();
      float radius =
          std::sqrt(lightDir.x() * lightDir.x() + lightDir.z() * lightDir.z());
      lightDir.x() = radius * cos(_lightAngle + i * M_PI / 2.f);
      lightDir.z() = radius * sin(_lightAngle + i * M_PI / 2.f);
      _pointLights[i]->transformationMatrix().translation() =
          _cam.sceneCenter() + _cam.sceneRadius() * lightDir;
    }
    _lightAngle += M_PI / 100.f;
    _lastTime = glfwGetTime();
  }

  if (_shadingMode == DEFERRED) {
    drawDeferred();
  } else {
    drawForward();
  }
}

void Viewer::loadProgram() {
  _blinnPrg.loadFromFiles(DATA_DIR "/shaders/blinn.vert",
                          DATA_DIR "/shaders/blinn.frag");
  _simplePrg.loadFromFiles(DATA_DIR "/shaders/simple.vert",
                           DATA_DIR "/shaders/simple.frag");
  _gbufferPrg.loadFromFiles(DATA_DIR "/shaders/gbuffer.vert",
                          DATA_DIR "/shaders/gbuffer.frag");
  _deferredPrg.loadFromFiles(DATA_DIR "/shaders/deferred.vert",
                          DATA_DIR "/shaders/deferred.frag");
}

void Viewer::updateGUI() {
  ImGui::RadioButton("Forward", (int *)&_shadingMode, FORWARD);
  ImGui::SameLine();
  ImGui::RadioButton("Deferred", (int *)&_shadingMode, DEFERRED);
  ImGui::Checkbox("Animate light", &_animate);
}

////////////////////////////////////////////////////////////////////////////////
// Events

/*
   callback to manage mouse : called when user press or release mouse button
   You can change in this function the way the user
   interact with the system.
 */
void Viewer::mousePressed(GLFWwindow *window, int button, int action) {
  if (action == GLFW_PRESS) {
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
      _cam.startRotation(_lastMousePos);
    } else if (button == GLFW_MOUSE_BUTTON_RIGHT) {
      _cam.startTranslation(_lastMousePos);
    }
    _button = button;
  } else if (action == GLFW_RELEASE) {
    if (_button == GLFW_MOUSE_BUTTON_LEFT) {
      _cam.endRotation();
    } else if (_button == GLFW_MOUSE_BUTTON_RIGHT) {
      _cam.endTranslation();
    }
    _button = -1;
  }
}

/*
   callback to manage mouse : called when user move mouse with button pressed
   You can change in this function the way the user
   interact with the system.
 */
void Viewer::mouseMoved(int x, int y) {
  if (_button == GLFW_MOUSE_BUTTON_LEFT) {
    _cam.dragRotate(Vector2f(x, y));
  } else if (_button == GLFW_MOUSE_BUTTON_RIGHT) {
    _cam.dragTranslate(Vector2f(x, y));
  }
  _lastMousePos = Vector2f(x, y);
}

void Viewer::mouseScroll(double x, double y) {
  _cam.zoom((y > 0) ? 1.1 : 1. / 1.1);
}

/*
   callback to manage keyboard interactions
   You can change in this function the way the user
   interact with the system.
 */
void Viewer::keyPressed(int key, int action, int mods) {
  if (key == GLFW_KEY_R && action == GLFW_PRESS) {
    loadProgram();
  } else if (key == GLFW_KEY_SPACE && action == GLFW_PRESS) {
    _animate = !_animate;
  } else if (key == GLFW_KEY_D && action == GLFW_PRESS) {
    _shadingMode = ShadingMode((_shadingMode + 1) % 2);
  }
}

void Viewer::charPressed(int key) {}
