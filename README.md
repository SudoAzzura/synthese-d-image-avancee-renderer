# TD1 - OpenGL Avancé - Chloé Neuville
## Partie 1 -  Deferred shading
### 1. Création et remplissage du G-buffer
Voici la texture des couleurs du FBO sur disque sous la forme d'image:


![alt text](./dataForMd/color.png)


Voici la texture des normales du FBO sur disque sous la forme d'image:


![alt text](./dataForMd/normal.png)

On peut même directement observer la normal en la mettant à la place de la couleur. 

![alt text](./dataForMd/NormalAPlaceColor.png)


Et vice-versa pour observer la couleur en la mettant à la place de la normal.


![alt text](./dataForMd/ColorAPlaceNormal.png)

Ce qui parfois en cas d'erreur nous fait voir ce genre d'image lorsqu'il y a un problème dans le gbuffer. Par exemple, lors de ce problème là :


![alt text](./dataForMd/problèmeGbuffer.png)
Où il y avait 
"out_normal_z.w = gl_FragCoord.w;" 
au lieu de 
"out_normal_z.w = gl_FragCoord.z;"
Les coordonnées de la profondeur étant alors incorrectes.

### 2. Calcul de l'éclairage
Le calcul de l'éclairage a été l'une de mes parties préférées. En ayant compris les fragments et vertex shader, je me suis empressée de le calculer pour plusieurs lumières.


![alt text](./dataForMd/EclairageFonctionnel.png)


J'ai créé des tableaux de position de lumière et de couleur de lumière dans blinn.frag pour le Forward et aussi dans deferred.frag pour y blend la lumière(mélanger) grace à phong. Et j'ai créer une boucle pour chacune des fonctions draw (drawDeferred() & drawForward()) pour que chacune envoie les informations des lumières. Cette partie a été un chalenge et une réussite personnelle remontant aux années de BTS SNIR que j'avais passé au paravant. Le fait de faire varier un "const char *" dans une boucle, peut-être que c'est simple pour d'autre mais pour ma part, c'était une première.
Mais l'une des plus grande frustration est le fait de devoir garder des #define pour le nombre des lumières et donc qu'à chaque fois que je modifie le nombre de lumière, je sois obligé d'aller le changer dans les fragments, ce qui peut être lassant surtout quand j'oublie et que je ne comprends pas l'erreur.


![alt text](./dataForMd/ArmyForward.png)
![alt text](./dataForMd/ArmyDeferred.png)


J'ai comparé les frames par seconde pour mon armée de TinkyWinkye, et quelques lumières. J'ai remarqué que lorsque ma grosse lumière rouge qui passe dans les TéléTubbies est dans le cadre de la caméra, le nombre de frame est supérieur en Forward. Alors que lorsqu'elle est hors de l'écran c'est en Deferred que c'est suppérieur. Peut-être est-ce le fait le blend en fragment plutôt que dan le viewer.cpp qui ralentis l'affichage.


## Partie 2 - Shadow Volumes
### 1. Construction des volumes d'ombres
Cette partie était un véritable casse-tête, j'y ai laissé un bout de mon âme. 
Je pense que depuis le début j'étais sur la bonne voie, mais je n'ai pas confiance en moi et donc j'ai énormément testé, pour arriver à des écrans noirs, des Segmentations fault, puis grâce à votre aide, ce fut une impultion vers le succés. Je vais essayer de détailler. Dans computeShadowVolume(const Vector3f &lightPos) de mech.cpp, j'ai créé une mesh à laquelle j'ai ajouteé la propriété "v:position" au nouveau maillage, ceci grâce à votre aide puis j'ai itéré sur les edges, dans cette partie vous m'avez donné des conseils, je n'ai pas tout écouté, car j'étais à l'aise avec la partie que j'avais réussie à utiliser.
J'ai extrait les halfedges, puis en ai extrait les vertex, grâce à eux j'ai formé des points. Qui m'ont servi a créer les vecteurs correspondant à la direction de la lumière en ces points. J'ai aussi extrait les deux faces de ces vecteurs, en ai extrait leur normal. J'ai fait le produit scalair de ces normal et des vecteur. Si l'un est positif et l'autre négatif alors il fait partiede l'ombre. C'est à dire si l'une des faces est éclairé et l'autre à l'ombre.
Si on fait comme si la dernière condition n'a pas d'importance, on dessine tous les triangles de la sphere dans le cas où j'ai choisi d'afficher que la silhouette de l'ombre ça donne ça :

![alt text](./dataForMd/VolumeShadowSansConditionSilhouette.png)


Dans celui où j'affiche le volume d'ombre ça donne ça. Les quads partent de toutes les faces.


![alt text](./dataForMd/VolumeShadowSansCondition.png)


Ensuite, si on respecte bien la condition, ça donne ça en sihouette:


![alt text](./dataForMd/silhouette.png)


Et ça en shadow volume comme désiré:


![alt text](./dataForMd/VolumeShadowFontionnel.png)

Mais si on oublie de bien déclarer la matrice de tranformation ça peut donner ça:


![alt text](./dataForMd/VolumeShadowMatrixTranformMauvaise.png)


L'ombre est décalée

Mais surtout, LE PROBLEME, c'est lorsque la lumière est en (0,0,0), dans ce cas là, la condition principale n'est pas respecté car lorsque l'on fait des calculs avec le "0", les résultats ne sont pas très riches, et ne varient pas : exemple :x-0 = x .


# Résultats positifs résumés :
Gbuffer : Fonctionnel
![alt text](./dataForMd/color.png)
![alt text](./dataForMd/normal.png)
Calcul d'éclairage & armée de TinkyWinky : Fonctionnel

![alt text](./dataForMd/ArmyForward.png)
![alt text](./dataForMd/ArmyDeferred.png)
Si vous voulez l'armée : 
 il suffira de décommenter là où il y a marqué "THE ARMY" dans le viewer.cpp
VolumeShadow : Fonctionnel

![alt text](./dataForMd/VolumeShadowFontionnel.png)